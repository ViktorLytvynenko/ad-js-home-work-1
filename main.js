///Theory

//1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
//Раскрывается и создаются определенные методы, которые могут использоваться для других объектов.
//2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
//Это функция вызывающая родительский конструктор.

///Practice

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(name) {
        this._name = name.trim();
    }

    set age(age) {
        this._age = age.trim();
    }

    set salary(salary) {
        this._salary = salary.trim();
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary
    }
}

let employee = new Employee(1, 2, 3)
employee.salary = "5"

class Programmer extends Employee {
    set lang(lang) {
        this._lang = lang.trim();
    }

    get lang() {
        return this._lang;
    }

    set salary(salary) {
        this._salary = salary.trim();
    }

    get salary() {
        return this._salary * 3;
    }
}

let programmers = [
    new Programmer("Viktor", 33, 1000),
    new Programmer("Andrew", 22, 2000),
    new Programmer("Max", 30, 3000)
]

for (let programmer of programmers) {
    console.log(`Name is ${programmer.name},`, `Age is ${programmer.age},`, `Salary is ${programmer.salary}`);
}


